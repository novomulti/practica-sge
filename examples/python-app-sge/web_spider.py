# -*- coding: utf-8 -*-
import requests, pdb, traceback
import os, json
from bs4 import BeautifulSoup

class LinkDownloader():
	links = []
	urlCall = None
	requestFile = None
	requestObject = None

	def __init__(self, urlCall):
		self.urlCall = urlCall
		print(u"Instanciado downloader")

	def get_request_from_url(self):
		response = requests.get(self.urlCall)
		self.requestFile = response
		return response

	def parse_request_file(self):
		soup = BeautifulSoup(self.requestFile.text, 'html.parser')
		self.requestObject = soup
		return soup

	def get_links(self, **parameters):
		anchors = self.requestObject.find_all('a')
		for a in anchors:
			if a.get('href') not in ['#','javascript:void(0)']:
				self.links.append(a.get('href'))
		return True

	def start_spider(self):
		self.get_request_from_url()
		self.parse_request_file()
		self.get_links()
		return self.links

	def itebooks(self):
		enlaces = []
		pages = 683
		for x in xrange(1, 683+1):
			if x == 1:
				url = "http://www.allitebooks.com"
			else:
				url = "http://www.allitebooks.com/page/%s/" % str(x)
			self.url = url
			print(u"Estamos en la pagina: %s" % str(x))
			r = self.get_request_from_url()
			soup = self.parse_request_file()
			for s in soup.find_all('h2', class_="entry-title"):
				enlaces.append({
						'title': s.a.text,
						'url': s.a.get('href')
					})
				self.links.append(s.a.get('href'))
		with open('data.json', 'w') as outfile:
			json.dump(enlaces, outfile)
		return self.links
		